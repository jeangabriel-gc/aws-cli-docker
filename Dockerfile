FROM python:3.7-stretch

RUN pip install awscli --upgrade \
  && apt-get update \
  && apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
  && apt-get update \
  && apt-get install docker-ce -y \
  && apt-get clean \
  && rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin \
  && echo "=================== DONE INSTALLING =======================" \
  && aws --version \
  && docker --version

